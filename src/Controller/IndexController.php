<?php

namespace App\Controller;

use App\Entity\FavoritePost;
use App\Entity\User;

use App\Form\AuthType;
use App\Form\RedditsType;
use App\Model\Api\ApiContext;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class IndexController extends Controller
{
    /**
     * @Route("/", name="index")
     * @param Request $request
     * @param ApiContext $apiContext
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Model\Api\ApiException
     */
    public function indexAction(Request $request, ApiContext $apiContext)
    {
        $form = $this->createForm(RedditsType::class);
        $form->handleRequest($request);

        $reddits = [];

        if ($form->isSubmitted() && $form->isValid()) {
           $searchBy = $form->getData();
           $reddits = $apiContext->showReddit($searchBy['q'], $searchBy['sort'], $searchBy['limit'])['data']['children'];
        }

        dump($reddits);

            return $this->render('index/index.html.twig', [
            'form' => $form->createView(),
                'reddits' => $reddits
        ]);
    }

    /**
     * @Route("/sign-up", name="app-sign-up")
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function signUpAction(
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $error = null;
        $user = new User();
        $form = $this->createForm("App\Form\RegisterType", $user);

        $form->handleRequest($request);


            if ($form->isSubmitted() && $form->isValid()) {

                    $data = [
                        'email' => $user->getEmail(),
                        'password' => $user->getPassword()
                    ];

                    $user = $userHandler->createNewUser($data);
                    $manager->persist($user);
                    $manager->flush();

                    $userHandler->makeUserSession($user);

                    return $this->redirectToRoute("index");
                }


        return $this->render('sign_up.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/auth", name="auth")
     * @param UserRepository $userRepository
     * @param Request $request
     * @param UserHandler $userHandler
     * @return Response
     *
     */
    public function authAction(
        UserRepository $userRepository,
        Request $request,
        UserHandler $userHandler
    )
    {
        $error = null;

        $form = $this->createForm(AuthType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $user = $userRepository->getByCredentials(
                $data['password'],
                $data['email']
            );

            if ($user) {
                $userHandler->makeUserSession($user);
                return $this->redirectToRoute('index');
            } else {
                $error = 'Ты не тот, за кого себя выдаешь';
            }

        }
        return $this->render(
            '/sign_in.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/show-one/{id}", name="show-one")
     * @param ApiContext $apiContext
     * @param string $id
     * @return Response
     * @throws \App\Model\Api\ApiException
     * @Route({"GET", "POST"})
     */
    public function showOne(ApiContext $apiContext, string $id){
        $data = $apiContext->getOneById($id)['data']['children'];

        $reddit = $data[0]['data'];


        return $this->render('show-one.html.twig', [
            'reddit' => $reddit,
        ]);
    }

    /**
     * @Route("/add-favorite/{id}", name="add-favorite")
     * @param $id
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addFavorite($id, ObjectManager $manager){
        $user = $this->getUser();

        $favoritePost = new FavoritePost();
        $favoritePost->setUser($user);
        $favoritePost->setReddit($id);

        $manager->persist($favoritePost);
        $manager->flush();

        return $this->redirectToRoute('index');
    }
}
