<?php


namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;



/**
 * @ORM\Entity(repositoryClass="App\Repository\FavoritePostRepository")
 */

class FavoritePost
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $reddit;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="favoritePosts")
     */
    private $user;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $reddit
     * @return FavoritePost
     */
    public function setReddit($reddit)
    {
        $this->reddit = $reddit;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReddit()
    {
        return $this->reddit;
    }

    /**
     * @param User $user
     * @return FavoritePost
     */
    public function setUser(User $user): FavoritePost
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}
