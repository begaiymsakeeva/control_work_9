<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RedditsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('q', TextType::class, ['label' => false])
            ->add('sort', ChoiceType::class,
                ['label' => false,
                    'choices' =>
        ['По релевантности' => 'relevance',
            'Горячие' => 'hot',
            'В топе' => 'top',
            'Новые' => 'new',
            'Комментируемые' => 'comments']
                ])
            ->add('limit', ChoiceType::class,
                ['label' => false,
                    'choices' =>
                    ['8шт' => 8,
                        '12шт' => 12,
                        '16шт' => 16,
                        '50шт'=> 50,
                        '100шт' => 100]

                ])
            ->add('submit', SubmitType::class, ['label' => 'Искать'])
        ;
    }


}
