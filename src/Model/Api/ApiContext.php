<?php

namespace App\Model\Api;

class ApiContext extends AbstractApiContext
{
    const ENDPOINT_REDDIT = 'https://www.reddit.com/r/picture/search.json';
    const ENPOINT_POST = "https://www.reddit.com/api/info.json";


    /**
     * @param string $q
     * @param string $sort
     * @param string $limit
     * @return mixed
     * @throws ApiException
     */
    public function showReddit(string $q, string $sort, string $limit){
        return $this->makeQuery(self::ENDPOINT_REDDIT, self::METHOD_GET,
            ['q' => $q,
                'sort' => $sort,
                'limit' => $limit,
                'type'=>'link']);
    }

    /**
     * @param string $id
     * @return array
     * @throws ApiException
     */
    public function getOneById(string $id): array
    {
        return $this->makeQuery(self::ENPOINT_POST, self::METHOD_GET, ['id' => $id]);
    }
    /**
     * @param string $password
     * @return string
     */
    public function encodePassword(string $password) {
        return md5($password).md5($password.'3');
    }
}
